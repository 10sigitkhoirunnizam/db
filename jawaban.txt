1. create database myshop

2. create table users(
- id int auto_increment,
- nama varchar (255),
- email varchar (255),
- password varchar (255),
- primary key (id)
);

   create table categories(
- id int auto_increment,
- primary key (id),
- name varchar (255)
);

   create table items(
- id int auto_increment,
- name varchar (255),
- description varchar (255),
- price int,
- stock int,
- category_id int,
- primary key(id),
- foreign key(category_id) references categories(id)
);

3. insert into users(nama,email,password) values
- ("John Doe","john@doe.com","john123"),
- ("John Doe","john@doe.com","jenita123");

   insert into categories(name) values ("gadget"),("cloth"),("men"),("women"),("branded");

   insert into items(name,description,price,stock,category_id) values
- ("Sumsang b50","hape keren dari merek sumsang","4000000","100","1"),
- ("Unikloh","baju keren dari merek ternama","500000","50","2"),
- ("IMHO Watch","jam tangan anak yang jujur banget","2000000","10","1");

4. a. select id,nama,email from users
   b1. select * from users harga > 1000000
   b2. select * from users where nama like 'uniklo'
   c. select name.items,description.items,price.items,stock.items,category_id.items,categories;

5. update items set price =  2500000 where id = 1;